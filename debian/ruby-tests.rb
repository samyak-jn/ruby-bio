Encoding.default_external = "UTF-8" if defined? Encoding

Dir['test/{unit,functional}/**/test_*.rb'].each { |f| require f }
